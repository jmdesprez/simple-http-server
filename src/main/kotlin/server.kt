
import org.http4k.core.ContentType
import org.http4k.core.Method.*
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.filter.CorsPolicy
import org.http4k.filter.DebuggingFilters
import org.http4k.filter.RequestFilters
import org.http4k.filter.ServerFilters
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.SunHttp
import org.http4k.server.asServer

fun main() {
    val app = routes(
        "bob" bind GET to {
            Response(OK)
                .body("you GET bob")
                .header("content-type", ContentType.TEXT_PLAIN.toHeaderValue())
        },
        "rita" bind POST to {
            Response(OK)
                .body("you POST rita")
                .header("content-type", ContentType.TEXT_PLAIN.toHeaderValue())
        },
        "sue" bind DELETE to {
            Response(OK)
                .body("you DELETE sue")
                .header("content-type", ContentType.TEXT_PLAIN.toHeaderValue())

        }
    )


    val filters = listOf(
        DebuggingFilters.PrintRequest(),
        DebuggingFilters.PrintResponse(),
        RequestFilters.Tap { println("hello") },
        RequestFilters.Tap { println("world") },
        ServerFilters.Cors(CorsPolicy.UnsafeGlobalPermissive)
    )

    filters.foldRight(app) { filter, acc -> acc.withFilter(filter) }.asServer(SunHttp(port = 8000)).start()
}