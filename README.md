# Simple HTTP4K Server

## Test with swagger

1. checkout this repository
1. start the `main` method in `server.kt`
1. go to [Online Swagger Editor](https://editor.swagger.io/) or use a local one
1. copy the content of [the specification](src/main/resources/swagger.yaml) to the editor

## Cors

The filter `ServerFilters.Cors(CorsPolicy.UnsafeGlobalPermissive)` is mandatory to allow
your browser to process the answer from the server.
